package metadata_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "gemnasium-python",
		Name:    "gemnasium-python",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python",
	}
	got := metadata.ReportScanner
	require.Equal(t, want, got)
}
