ARG DS_PYTHON_VERSION=3.6

FROM golang:1.17-alpine AS build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM python:$DS_PYTHON_VERSION-slim

COPY gemnasium/vrange/python /vrange/python
ENV VRANGE_DIR="/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

ARG DS_GET_PIP_PATH="/get-pip.py"
ENV DS_GET_PIP_PATH $DS_GET_PIP_PATH

ARG PIPENV_DEFAULT_PYTHON_VERSION="3"
ENV PIPENV_DEFAULT_PYTHON_VERSION $PIPENV_DEFAULT_PYTHON_VERSION

ADD get-pip.py requirements.txt /

RUN \
	# include gcc build env, and upgrade packages
	apt-get update && \
	apt-get install -y wget git build-essential libssl-dev libffi-dev musl-dev python3-dev && \
	apt-get upgrade -y && \
	rm -rf /var/lib/apt/lists/* && \
	\
	# install pipenv and its requirements
	pip install -r /requirements.txt && \
	\
	# install vrange/python
	pip install -r $VRANGE_DIR/python/requirements.txt && \
	\
	# clone gemnasium-db
	git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH && \
	\
	# give write access to CA certificates (OpenShift)
	mkdir -p /etc/ssl/certs/ && \
        touch /etc/ssl/certs/ca-certificates.crt && \
	chmod g+w /etc/ssl/certs/ca-certificates.crt && \
	\
	# give write access to vulnerability database (OpenShift)
	chmod -R g+w $GEMNASIUM_DB_LOCAL_PATH && \
	echo "done"

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"

# By default, set "package_manager" field of generated reports to "pip" regardless of the package manager being used.
# This is needed to maintain backward compatibility with older versions of GitLab.
# This default value cannot be set in the Go project because `DS_REPORT_PACKAGE_MANAGER_PIP_WHEN_PYTHON`
# isn't handled using https://pkg.go.dev/github.com/urfave/cli/v2, because convert.Convert doesn't support this.
ENV DS_REPORT_PACKAGE_MANAGER_PIP_WHEN_PYTHON="true"

COPY --from=build /analyzer /analyzer
ENTRYPOINT []
CMD ["/analyzer", "run"]
