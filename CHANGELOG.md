# Gemnasium Python analyzer changelog

## v2.19.3
- Remove `PIP_EXTRA_INDEX_URL` from the log (!132)

## v2.19.2
- Fix a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!130)

## v2.19.1
- Show INFO message when a single directory is scanned, and other directories are skipped (!126)

## v2.19.0
- Use `DS_EXCLUDED_PATHS` variable to filter out paths prior to the scan (!128)

## v2.18.7
- Fix ignored CLI flags when configuring the `pip` builder. (!124)

## v2.18.6
- Change `package_manager` field of JSON reports from `pip` to `pipenv` when scanning Pipenv projects. This behavior is enabled by forcing the `DS_REPORT_PACKAGE_MANAGER_PIP_WHEN_PYTHON` variable to `"false"`. (!121)

## v2.18.5
- Upgrade to Go 1.17 (!122)

## v2.18.4
- Output warning when `PIP_EXTRA_INDEX_URL` var is configured (!105)

## v2.18.3
- Update to Security Report Schema `v14.0.0` (!102)

## v2.18.2
- Fix ignored `PIP_REQUIREMENTS_FILE` variable (!99)

## v2.18.1
- Change pipenv builder to try install that is not destructive to virtualenv (!98)

## v2.18.0
- Change permissions for Red Hat OpenShift compatibility (!96)

## v2.17.3
- Fix error when issuing git pull on remote-only branch (!94)

## v2.17.2
- Update go-cvss to v0.4.0 (!87)

## v2.17.1
- Fix `no parser` error when Pipenv project has both `Pipfile` and `Pipfile.lock` (!83)
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!83)

## v2.17.0
- Detect supported projects, and scan no more than one Python project per directory (!76)

## v2.16.0
- Update Go dependencies `common` and `urfave/cli` to remediate vulnerability GMS-2019-2 (!74)

## v2.15.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!63)

## v2.14.1
- Update common to `v2.14.0` which allows git to use CA Certificate bundle (!62)

## v2.14.0
- Add scan object to report (!61)

## v2.13.0
- Update logging to be standardized across analyzers (!59)

## v2.12.1
- Fix link to advisory (!58)

## v2.12.0
- Remove deprecated `PIP_DEPENDENCY_PATH` environment varable (!56)

## v2.11.0
- Add support for using `ADDITIONAL_CA_CERT_BUNDLE` as `PIP_CERT` (!55)

## v2.10.0
- Update gemnasium to `v2.10.0` which adds support for vulnerability Severity levels

## v2.9.0
- Add `id` field to vulnerabilities in JSON report (!52)

## v2.8.1
- Fix flaky requirements for pipenv (!49)

## v2.8.0
- Switch from [Alpine Linux](https://www.alpinelinux.org/)
  to [Debian Buster slim](https://packages.debian.org/buster/slim)
  in order to be [manylinux2010](https://www.python.org/dev/peps/pep-0571/#the-manylinux2010-policy) compliant (!37)

## v2.7.0
- Add support for custom CA certs (!48)

## v2.6.0
- Add `PIP_REQUIREMENTS_FILE` variable, which allows the user to specify the pip requirements file to scan (!33)

## v2.5.0
- Add `DS_PIP_VERSION` flag allowing users to specify the version of pip that the analyzer will use (!21)

## v2.4.0
- Use gemnasium-db git repo instead of the Gemnasium API (!29)

## v2.3.0
- Add `setup.py` support (!27)

## v2.2.4
- Rename `PIP_DEPENDENCY_PATH` to `DS_PIP_DEPENDENCY_PATH` (!24)

## v2.2.3
- Fix `DS_EXCLUDED_PATHS` not applied to dependency files (!21)

## v2.2.2
- Fix dependency list, include dependency files which do not have any vulnerabilities (!14)

## v2.2.1
- Sort the dependency files and their dependencies (!13)

## v2.2.0
- List the dependency files and their dependencies (!12)

## v2.1.0
- Extend the base python image with gcc build environment for pip wheels (@janw)
- Lock to python3.6
- Add `PIP_DEPENDENCY_PATH` EnvVar for relying on local dependencies

## v2.0.2
- Bump common to v2.1.6
- Bump gemnasium to v2.1.2

## v2.0.1
- Bump common to v2.1.5, introduce remediations
- Bump gemnasium to v2.1.1, introduce stable report order
- Fix `Pipfile` support by using `pipenv`

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
