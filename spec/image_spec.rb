require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'

describe "python-3.9 image", python_version: '3.9' do
  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium-python:3.9-latest")
  end

  it "runs python 3.9 by default" do
    output = `docker run -t --rm #{image_name} python --version`
    expect(output).to match("Python 3.9")
  end
end

describe "running image" do
  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium-python:latest")
  end

  context "with no project" do
    before(:context) do
      @output = `docker run -t --rm -w /app #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it "outputs analyzer version" do
      analyzer_version = "2.19.3"
      expect(@output).to match(/GitLab gemnasium-python analyzer v#{analyzer_version}/i)
    end

    it "shows there is no match" do
      expect(@output).to match(/no match in \/app/i)
    end

    describe "exit code" do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context "with test project" do
    def fixtures_dir
      File.expand_path("../qa/fixtures", __dir__)
    end

    def remove_excluded_packages report
      filtered_packages = %w(pip setuptools wheel)
      report.tap do |report|
        report['dependency_files'] = (report['dependency_files'] || []).map do |depfile|
          depfile.tap do |df|
            df['dependencies'].reject! { |d| filtered_packages.include?(d['package']['name'])  }
          end
        end
      end
      report
    end

    def parse_expected_report(expectation_name, report_filename: "gl-dependency-scanning-report.json")
      base_dir = File.expand_path("../qa/expect", __dir__)
      path = File.join(base_dir, expectation_name, report_filename)
      remove_excluded_packages(JSON.parse File.read path)
    end

    let(:global_vars) do
      {
        "GEMNASIUM_DB_REF_NAME": "v1.2.142",
        "SECURE_LOG_LEVEL": "debug",
        "DS_REPORT_PACKAGE_MANAGER_PIP_WHEN_PYTHON": "false",
      }
    end

    let(:project) { "any" }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }

    let(:scan) do
      target_dir = File.join(fixtures_dir, project)
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, target_dir,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables))
    end

    let(:report) { remove_excluded_packages(scan.report) }

    context "using pip" do
      context "with requirements.txt" do
        let(:project) { "python-pip/no-pillow" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["requirements.txt"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "python-pip/no-pillow" }
          end

          it_behaves_like "valid report"
        end
      end

      context "with custom requirements filename" do
        let(:project) { "python-pip/no-pillow-custom-req-file" }

        let(:variables) do
          { "PIP_REQUIREMENTS_FILE": "cust-reqs.foo" }
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["cust-reqs.foo"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "python-pip/no-pillow-custom-req-file" }
          end

          it_behaves_like "valid report"
        end
      end
    end

    context "using Pipenv" do
      context "without a lock file" do
        let(:project) { "python-pipenv/master" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["Pipfile"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "python-pipenv" }
          end

          it_behaves_like "valid report"
        end
      end

      context "with a lock file" do
        let(:project) { "python-pipenv/pipfile-lock" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["Pipfile"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "python-pipenv/pipfile-lock" }
          end

          it_behaves_like "valid report"
        end
      end

      context "when DS_REPORT_PACKAGE_MANAGER_PIP_WHEN_PYTHON is true" do
        let(:project) { "python-pipenv/master" }

        let(:variables) do
          { "DS_REPORT_PACKAGE_MANAGER_PIP_WHEN_PYTHON": "true" }
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["Pipfile"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "python-pipenv/package-manager-pip" }
          end

          it_behaves_like "valid report"
        end
      end
    end

    context "using Setuptools" do
      context "with no requirements.txt file" do
        let(:project) { "python-setuptools/main" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["setup.py"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "python-setuptools" }
          end

          it_behaves_like "valid report"
        end
      end

      context "with excluded requirements.txt file" do
        let(:project) { "python-setuptools/requirements-file" }

        it_behaves_like "successful scan"

        let(:variables) do
          { "DS_EXCLUDED_PATHS": "/requirements.txt" }
        end

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["setup.py"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "python-setuptools" }
          end

          it_behaves_like "valid report"
        end
      end
    end
  end
end
