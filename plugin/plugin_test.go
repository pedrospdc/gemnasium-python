package plugin

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestMatch(t *testing.T) {
	cases := []struct {
		envReqFile string
		path       string
		fileInfo   fakeFileInfo
		want       bool
	}{
		{
			envReqFile: "other/reqs.txt",
			path:       "/tmp/project/other/reqs.txt",
			fileInfo:   fakeFileInfo{"reqs.txt"},
			want:       true,
		},
		{
			envReqFile: "reqs.txt",
			path:       "/tmp/project/other/reqs.txt",
			fileInfo:   fakeFileInfo{"reqs.txt"},
			want:       true,
		},
		{
			envReqFile: "prod/requirements.txt",
			path:       "/tmp/project/stage/requirements.txt",
			fileInfo:   fakeFileInfo{"requirements.txt"},
			want:       false,
		},
		{
			envReqFile: "",
			path:       "/tmp/project/other/reqs.txt",
			fileInfo:   fakeFileInfo{"reqs.txt"},
			want:       false,
		},
		{
			envReqFile: "",
			path:       "",
			fileInfo:   fakeFileInfo{"requirements.txt"},
			want:       true,
		},
		{
			envReqFile: "",
			path:       "",
			fileInfo:   fakeFileInfo{"requirements.pip"},
			want:       true,
		},
		{
			envReqFile: "",
			path:       "",
			fileInfo:   fakeFileInfo{"Pipfile"},
			want:       true,
		},
		{
			envReqFile: "",
			path:       "",
			fileInfo:   fakeFileInfo{"requires.txt"},
			want:       true,
		},
		{
			envReqFile: "",
			path:       "",
			fileInfo:   fakeFileInfo{"setup.py"},
			want:       true,
		},
	}
	for i, c := range cases {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			err := os.Setenv(envPipRequirementsFile, c.envReqFile)
			defer os.Unsetenv(envPipRequirementsFile)
			require.NoError(t, err, "unexpected error setting env")
			got, err := Match(c.path, c.fileInfo)
			require.NoError(t, err)
			require.Equalf(t, c.want, got, "expected Match result for\n%v\nto return %t but got %t", c, c.want, got)
		})
	}
}

type fakeFileInfo struct {
	file string
}

func (f fakeFileInfo) Name() string {
	return f.file
}

func (f fakeFileInfo) Size() int64 {
	return 0
}

func (f fakeFileInfo) Mode() os.FileMode {
	return 0
}

func (f fakeFileInfo) ModTime() time.Time {
	return time.Now()
}

func (f fakeFileInfo) IsDir() bool {
	return false
}

func (f fakeFileInfo) Sys() interface{} {
	return nil
}
